<?php
//	error_reporting(-1);
//	ini_set("display_errors", 1);

/*
  $Id: invoice_pdf.php,v 1.0 2005/06/23 00:29:30 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

  Changelog: by Infobroker
  info@cooleshops.de
 */

define('FPDF_FONTPATH', 'fpdf/font/');
require('fpdf/fpdf.php');

require('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_INVOICE_PDF);

/**
 * 
 * @param type $raw_date
 * @param type $typ ( year | month | day )
 * @param int $add
 * @return string or false
 */
function tep_date_short_add($raw_date, $typ, $add) {
	if (($raw_date == '0000-00-00 00:00:00') || ($raw_date == ''))
		return false;

	if ($typ == 'year') {
		$year = substr($raw_date, 0, 4);
		$year = $year + (int) $add;
	} else {
		$year = substr($raw_date, 0, 4);
	}

	if ($typ == 'month') {
		$month = (int) substr($raw_date, 5, 2);
		$month = $month + (int) add;
	} else {
		$month = (int) substr($raw_date, 5, 2);
	}

	if ($typ == 'day') {
		$day = (int) substr($raw_date, 8, 2);
		$day = $day + (int) $add;
	} else {
		$day = (int) substr($raw_date, 8, 2);
	}

	$hour = (int) substr($raw_date, 11, 2);
	$minute = (int) substr($raw_date, 14, 2);
	$second = (int) substr($raw_date, 17, 2);

	if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
		return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
	} else {
		return ereg_replace('2037' . '$', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
	}
}

/**
 * Calculator Class to calculate taxes, rates and sums
 * @author Sebastian.Braun@ateliervision.de
 */
class Invoice2 {
	private $order;
	private $taxSums;
	private $taxRateForShipping;
	private $shipping;
	/**
	 * 
	 * @param object $order
	 */
	public function __construct($order) {
		$this->order = $order;
	}
	/**
	 * Groups products of the order by tax and return their sums grouped by tax-rate
	 * @return array e.g.( 19 => 119, 7 => 107)
	 */
	public function getTaxSums(){
		if ($this->taxSums)
			return $this->taxSums;
		$this->taxSums	= array();
		$sumsGroupedByTax	= array();
		foreach ($this->order->products as $p){
			$tax = round($p['tax'], 2);
			$sumsGroupedByTax	[$tax][] = $p['price'] * $p['qty'];
		}
		foreach ($sumsGroupedByTax as $rate => $productSum){
			$this->taxSums[$rate] = array_sum($sumsGroupedByTax[$rate]);
		}
		ksort($this->taxSums);
		return $this->taxSums;
	}
	
	public function getTaxRateForShipping(){
		if ($this->taxRateForShipping)
			return $this->taxRateForShipping;
		$ts = $this->getTaxSums();
		arsort($ts);
		reset($ts);
		$this->taxRateForShipping = key($ts);
		return $this->taxRateForShipping;
	}
	
	public function setVersandkosten($assoc_array){
		$this->versandkosten = $assoc_array;
		$b = $this->versandkosten['value'];
		$taxrate = $this->getTaxRateForShipping();
		$n = $b / (1+($taxrate/100));
		$tax = $n * (($taxrate/100));
		$this->shipping = array('netto' => $n, 'brutto' => $b, 'tax' => $tax, 'taxrate' => $taxrate);

	}
	/**
	 * return Shipping Values
	 * need: setVersandkosten
	 * @return array e.g. ('netto' => 1, 'brutto' => 1.07, 'tax' => 0.07, 'taxrate' => 7)
	 */
	public function getShipping(){
		return $this->shipping;
	}
}


require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

include(DIR_WS_CLASSES . 'order.php');

while (list($key, $oID) = each($_GET)) {
	if ($key != "oID")
		break;
	$orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . $oID . "'");
	$order = new order($oID);
	$invoice2 = new Invoice2($order);

// Kundennummer in invoice.php einfuegen
	$the_extra_query = tep_db_query("select * from " . TABLE_ORDERS . " where orders_id = '" . tep_db_input($oID) . "'");
	$the_extra = tep_db_fetch_array($the_extra_query);
	$the_customers_id = $the_extra['customers_id'];
	$the_extra_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " where customers_id = '" . $the_customers_id . "'");
	$the_extra = tep_db_fetch_array($the_extra_query);
	$the_customers_fax = $the_extra['customers_fax'];
// Ende Kundennummer in invoice.php
	$bruttosumme_query = tep_db_query("select * from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $oID . "' and class = 'ot_total'");
	$bruttosumme = tep_db_fetch_array($bruttosumme_query);
	$bruttoware = $bruttosumme['value'];
	$bruttotax_query = tep_db_query("select * from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $oID . "' and title like '%7%'");
	$bruttotax = tep_db_fetch_array($bruttotax_query);
	$tax1 = $bruttotax['value'];
	$bruttotax1_query = tep_db_query("select * from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $oID . "' and title like '%16%'");
	$bruttotax1 = tep_db_fetch_array($bruttotax1_query);
	$tax2 = $bruttotax1['value'];
	$tax_total = ($tax1 + $tax2);
	$versandkosten_query = tep_db_query("select * from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . $oID . "' and class = 'ot_shipping'");
	$versandkosten = tep_db_fetch_array($versandkosten_query);
	
	$invoice2->setVersandkosten($versandkosten);
	
/* depricated */
//	$versand = ($versandkosten['value'] / 1.07);
//	$netto = (($bruttoware - $tax_total) - $versand);

	$text_query = tep_db_query("SELECT * FROM edit_invoice where edit_invoice_id = '1' and language_id = '" . $languages_id . "'");
	$text = tep_db_fetch_array($text_query);

	$text2_query = tep_db_query("SELECT * FROM edit_invoice where edit_invoice_id = '2' and language_id = '" . $languages_id . "'");
	$text2 = tep_db_fetch_array($text2_query);
	
	
	class PDF extends FPDF {

	// Seitenheader
		function Header() {
			global $oID;
			$date = strftime('%A, %d %B %Y');
			// Logo
			if (EDIT_INVOICE_LOGO_ALIGN == 'rechts' and EDIT_INVOICE_LOGO != '') {
				$this->Image('images/oscommerce.jpg', 140, 20, 60);
			} else {
				if (EDIT_INVOICE_LOGO_ALIGN == 'links' and EDIT_INVOICE_LOGO != '') {
					$this->Image('images/oscommerce.jpg', 20, 20, 60);
				} else {
					if (EDIT_INVOICE_LOGO_ALIGN == 'mitte' and EDIT_INVOICE_LOGO != '') {
						$this->Image('images/oscommerce.jpg', 20, 20, 180);
					} else {
						if (EDIT_INVOICE_LOGO == '') {
							$this->Image('images/pixel_trans.jpg', 20, 20, 60);
						}
					}
				}
			}
		}

	// Seitenfooter
		function Footer() {
			global $pdf;
			$this->SetY(-34);
			$this->SetX(20);
			$footer_color_cell = explode(",", FOOTER_CELL_BG_COLOR);
			$this->SetFillColor($footer_color_cell[0], $footer_color_cell[1], $footer_color_cell[2]);
			$this->MultiCell(0, 0, "", 0, 'L', 1);

			$x = 20;
			$y = $this->GetY();
			$this->SetLineWidth(0.25);
			$this->Line($x, $y, $this->w - $this->rMargin, $y);

			$pdf->SetFont('Arial', '', 6);
			$this->SetY(-33);
			$pdf->SetX(19);
			$pdf->Cell(45, 3, SHOPBETREIBER, 0, 0, 'L', 0);
			$pdf->SetX(120);
			$pdf->Cell(45, 3, str_replace('Gerichtsstand: L�rrach', '', AMTSGERICHT), 0, 0, 'L', 0);
			$pdf->Ln();

			$pdf->SetFont('Arial', '', 6);
			$this->SetY(-30);
			$pdf->SetX(19);
			$pdf->Cell(45, 3, SHOPSTRASSE, 0, 0, 'L', 0);
			$pdf->SetX(119.2);
			$pdf->Cell(45, 3, str_replace('Inhaberin: Jasmin Horrelt', '', AMTSGERICHT), 0, 0, 'L', 0);
			$pdf->Ln();

			$pdf->SetFont('Arial', '', 6);
			$this->SetY(-27);
			$pdf->SetX(19);
			$pdf->Cell(45, 3, SHOPSTADT, 0, 0, 'L', 0);
			$pdf->SetX(120);
			$pdf->Cell(45, 3, OWNER_BANK_FA, 0, 0, 'L', 0);
			$pdf->SetX(75);
			$pdf->Cell(45, 3, '', 0, 0, 'L', 0);
			$pdf->Ln();

			$pdf->SetFont('Arial', '', 6);
			$this->SetY(-24);
			$pdf->SetX(19);
			$pdf->Cell(45, 3, SHOPTELEFON, 0, 0, 'L', 0);
			$pdf->SetX(120);
			$pdf->Cell(45, 3, OWNER_BANK_UST_NUMBER, 0, 0, 'L', 0);
			$pdf->Ln();

			$pdf->SetFont('Arial', '', 6);
			$this->SetY(-21);
			$pdf->SetX(19);
			$pdf->Cell(45, 3, SHOPFAX, 0, 0, 'L', 0);
			$pdf->SetX(75);
			$pdf->Cell(45, 3, OWNER_BANK_TAX_NUMBER, 0, 0, 'L', 0);
			$pdf->Ln();

			$pdf->SetFont('Arial', '', 6);
			$this->SetY(-18);
			$pdf->SetX(19);
			$pdf->Cell(45, 3, SHOPEMAIL, 0, 0, 'L', 0);
			$pdf->SetX(120);
			$pdf->Cell(45, 3, '', 0, 0, 'L', 0);
			$pdf->SetX(165);
			$pdf->Cell(45, 3, '', 0, 0, 'L', 0);
			$pdf->Ln();
			$pdf->Cell(0, 20, 'Seite ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
		}

	}

	/*
	  function Footer() {
	  $this->SetY(-19);
	  }
	 */

	
	
// �bernahme class
	$pdf = new PDF();
	$pdf->AliasNbPages();
// Abst�nde auf der seite setzen
	$pdf->SetMargins(4, 2, 4);

// Hinzuf�gen der 1. Seite
	$pdf->AddPage();

// Adressfeld mit Absender und Rechnungsanschrift
	$pdf->SetX(0);
	$pdf->SetY(58);
	$pdf->SetFont('Arial', '', 6);
	$pdf->SetTextColor(0);
	$pdf->Text(20, 50, SHOPBETREIBER . ' - ' . SHOPSTRASSE . ' - ' . SHOPSTADT);

	$pdf->SetX(200);
	$pdf->SetY(58);
	$pdf->SetFont('Arial', '', 6);
	$pdf->SetTextColor(0);
	$pdf->Text(145, 50, 'Lieferadresse');
	$pdf->SetFont('Arial', '', 6);
	$pdf->SetTextColor(0);
	$pdf->Text(25, 57, 'Rechnungsadresse');

	$pdf->SetFont('Arial', '', 9);
	$pdf->SetTextColor(0);
	$pdf->Cell(20);
	if (trim(strlen(tep_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br>')) == 0)) {
		$pdf->MultiCell(70, 3.3, html_entity_decode(tep_address_format($order->delivery['format_id'], $order->delivery, '', '', "\n")), 0, 'L');
	} else {
		$pdf->MultiCell(70, 3.3, html_entity_decode(tep_address_format($order->billing['format_id'], $order->billing, '', '', "\n")), 0, 'L');
	}

//Draw the invoice delivery address text
	$pdf->SetFont('Arial', '', 9);
	$pdf->SetTextColor(0);
	/* 	$pdf->Text(127,71,ENTRY_SHIP_TO); */
	$pdf->SetX(0);
	$pdf->SetY(58);
	$pdf->Cell(140);
	$pdf->MultiCell(50, 3.3, html_entity_decode(tep_address_format($order->delivery['format_id'], $order->delivery, '', '', "\n")), 0, 'L');

// Lieferanschrift
	/*
	  $pdf->SetFont('Arial','B',8);
	  $pdf->SetTextColor(0);
	  $pdf->Text(117,71,ENTRY_SHIP_TO);
	  $pdf->SetX(0);
	  $pdf->SetY(74);
	  $pdf->Cell(115);
	 */

// Barcode   
	/* if (BARCODE_RECHNUNG == 'ja'){
	  $print_barcode = pdf_open_jpeg($pdf, 'barcodegen.php?barcode= ' . tep_db_input($oID) .);
	  $pdf->Image($print_barcode,120,74,60);
	  }
	  $bar = barcodegen.php?barcode=
	  $pdf->Cell(120, 60, $bar,0,'L');
	  $pdf->Image(barcode.php?barcode=123456,120,74,60);
	  $pdf->Text(20,113, $bar . tep_db_input($oID));
	  $pdf-><IMG SRC="barcode.php?barcode=123456&width=320&height=200">
	 */
	if (BARCODE_RECHNUNG == 'ja') {
		$pdf->Code39(140, 50, tep_db_input($oID));
	}
	/*
	  barcodegen.php?barcode= ' . tep_db_input($oID) . ' ;
	 */

// Titel = Rechnung
	$pdf->SetFont('Arial', 'B', 8);
	$pdf->SetTextColor(0);
	$temp = substr(str_replace("<b>", '', (str_replace("</b>", '', $order->info['payment_method']))), 0, 30);
	$pdf->Text(20, 87, RECHNUNG);
// Zahlungsweise	
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$temp = substr(str_replace("<b>", '', (str_replace("</b>", '', $order->info['payment_method']))), 0, 30);
	$pdf->Text(20, 91, ENTRY_PAYMENT_METHOD . ' ' . $temp);
// Rechnungsnummer
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$temp = str_replace('&nbsp;', ' ', PRINT_INVOICE_ORDERNR);
	$pdf->Text(20, 95, $temp . tep_db_input($oID));
// Kundenummer
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$temp = str_replace('&nbsp;', ' ', ENTRY_INVOICE_COSTUMER_ID);
	$pdf->Text(20, 99, $temp . $the_customers_id);
// Rechnungsdatum
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$temp = str_replace('&nbsp;', ' ', PRINT_INVOICE_DATE);
	$pdf->Text(161, 95, $temp . tep_date_short($order->info['date_purchased']));
// F�lligkeitsdatum
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$temp2 = str_replace('&nbsp;', ' ', ENTRY_INVOICE_DATE_ZAHLBAR);
	$pdf->Text(161, 99, 'Rechnungsdatum ist Lieferdatum');
//$pdf->Text(171,99,$temp2 . tep_date_short_add($order->info['date_purchased'], 'day' , ZAHLUNGSFAELLIGKEIT)); 
// Betrag netto
//$pdf->SetFont('Arial','',8);
//$pdf->SetTextColor(0);
//$pdf->Text(139,165.3, 'Warenwert netto: ');
//$pdf->Text(186.8,165.3, $currencies->format($netto, $order->info['currency']));
// Falzmarke
	$pdf->SetY(105);
	$pdf->SetX(20);
	$x = 10;
	$y = $pdf->GetY();
	$pdf->SetLineWidth(0.25);
// $pdf->Line($x,$y,$this->w-$this->rMargin,$y);
// Rechnungstext 1
//if ($order->info['payment_method'] == '<b>Rechnung</b>') {
//$pdf->SetFont('Arial','',8);
//$pdf->SetTextColor(0);
//$pdf->SetY(104);
//    $pdf->Cell(15);
//   $text['edit_invoice_text'] = str_replace("<br>",'',$text['edit_invoice_text']);
//$pdf->MultiCell(200, 4,$text['edit_invoice_text'],0,'L');
//}
//$pdf->SetFont('Arial','B',8);
//$pdf->SetTextColor(0);
// Auftragsnummer
//$temp = str_replace('&nbsp;', ' ', ENTRY_INVOICE_AUFTRAG_ID);
//$pdf->Text(20,120, $temp . tep_db_input($oID)); 
// Fields Name position
//	$Y_Fields_Name_position = 125;//old
	$Y_Fields_Name_position = 112;
// Table position, under Fields Name
	//$Y_Table_Position = 131;
	$Y_Table_Position = $Y_Fields_Name_position + 6;

	function output_table_heading($Y_Fields_Name_position) {
		global $pdf;
	// Feldnamen der Rechnung
	// Hintergrundfarbe der Boxen
		$pdf->SetFillColor(232, 232, 232);
	// Schrift der Boxen
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->SetY($Y_Fields_Name_position);
		$pdf->SetX(20);
		$pdf->Cell(10, 6, TABLE_HEADING_QUANTITY, 1, 0, 'C', 1);
		$pdf->SetX(30);
		$pdf->Cell(25, 6, TABLE_HEADING_PRODUCTS_MODEL, 1, 0, 'C', 1);
		$pdf->SetX(55);
		$pdf->Cell(97, 6, TABLE_HEADING_PRODUCTS, 1, 0, 'C', 1);
		$pdf->SetX(152);
		$pdf->Cell(13, 6, TABLE_HEADING_TAX, 1, 0, 'C', 1);
		$pdf->SetX(125);
	//	$pdf->Cell(20,6,TABLE_HEADING_PRICE_EXCLUDING_TAX,1,0,'C',1);
		$pdf->SetX(165);
		$pdf->Cell(20, 6, TABLE_HEADING_PRICE_INCLUDING_TAX, 1, 0, 'C', 1);
		$pdf->SetX(165);
	//	$pdf->Cell(20,6,TABLE_HEADING_TOTAL_EXCLUDING_TAX,1,0,'C',1);
		$pdf->SetX(185);
		$pdf->Cell(18, 6, TABLE_HEADING_TOTAL_INCLUDING_TAX, 1, 0, 'C', 1);
		$pdf->Ln();
	}

	output_table_heading($Y_Fields_Name_position);

// Rechnungsaufgliederung nach Positionen
	for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
		$pdf->SetFont('Arial', '', 7);
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(20);
		//$temp = str_replace('&nbsp;', ' ');
		$pdf->MultiCell(10, 6, $order->products[$i]['qty'] . 'x', 1, 'C');
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(55);
		if (strlen($order->products[$i]['name']) > 40 && strlen($order->products[$i]['name']) < 45) {
			$pdf->SetFont('Arial', '', 7);
			$order->products[$i]['name'] = str_replace("<br>", '', $order->products[$i]['name']);
			$order->products[$i]['name'] = str_replace("<b>", '', $order->products[$i]['name']);
			$order->products[$i]['name'] = str_replace("</b>", '', $order->products[$i]['name']);
			$pdf->MultiCell(97, 6, $order->products[$i]['name'], 1, 'L');
		} else if (strlen($order->products[$i]['name']) > 45) {
			$pdf->SetFont('Arial', '', 7);
			$order->products[$i]['name'] = str_replace("<br>", '', $order->products[$i]['name']);
			$order->products[$i]['name'] = str_replace("<b>", '', $order->products[$i]['name']);
			$order->products[$i]['name'] = str_replace("</b>", '', $order->products[$i]['name']);
			$pdf->MultiCell(97, 6, substr($order->products[$i]['name'], 0, 45), 1, 'L');
		} else {
			$order->products[$i]['name'] = str_replace("<br>", '', $order->products[$i]['name']);
			$order->products[$i]['name'] = str_replace("<b>", '', $order->products[$i]['name']);
			$order->products[$i]['name'] = str_replace("</b>", '', $order->products[$i]['name']);
			$pdf->MultiCell(97, 6, $order->products[$i]['name'], 1, 'L');
		}
		$pdf->SetFont('Arial', '', 7);
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(152);
		$pdf->MultiCell(13, 6, tep_display_tax_value($order->products[$i]['tax']) . '%', 1, 'C');
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(30);
		$pdf->SetFont('Arial', '', 7);
		$pdf->MultiCell(25, 6, $order->products[$i]['model'], 1, 'C');
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(125);
		$pdf->SetFont('Arial', '', 7);
//  $pdf->MultiCell(20,6,$currencies->format($order->products[$i]['final_price'], true, $order->info['currency'], $order->info['currency_value']),1,'C');
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(155);
// $pdf->MultiCell(20,6,$currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']), true, $order->info['currency'], $order->info['currency_value']),1,'C');
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(165);
		$pdf->MultiCell(20, 6, $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']), 1, 'C');
		$pdf->SetY($Y_Table_Position);
		$pdf->SetX(185);
		$pdf->MultiCell(18, 6, $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']), 1, 'C');
		$Y_Table_Position += 6;

// Anzahl der Produkte checken, wenn mehr als 20 sind
		if ($i == 20) { /*hier wird nach genau 20, nicht nach "mehr als 20" abgefragt ... Fehler? */
			$pdf->AddPage();
// Position der Feldnamen auf den Folgeseiten
			$Y_Fields_Name_position = 125;
// Tabellenposition under den Feldnamen
			$Y_Table_Position = 70;
			output_table_heading($Y_Table_Position - 6);
		}
	}

	$Y_Table_Position += 4;

	if ($Y_Table_Position > 240) {
		$pdf->AddPage();
		$Y_Table_Position = 70;
	}

	for ($i = 0, $n = sizeof($order->totals); $i < $n; $i++) {
		if ($i == 0) {
// Text Zahlungsweise
//  $pdf->SetFont('Arial','',8);
//  $pdf->SetTextColor(0); 
//  $temp = substr (str_replace("<b>",'',(str_replace("</b>",'',$order->info['payment_method']))) , 0, 30);
//  $pdf->Text(20,$Y_Table_Position-56,ENTRY_PAYMENT_METHOD . ' ' . $temp); 
//  $pdf->SetFont('Arial','',8);
		}
		if (strstr($order->totals[$i]['title'],"(enthaltene MwSt.")){
			unset($order->totals[$i]);
			break;
		}
		
		$pdf->SetY(($Y_Table_Position + 2));
		// Position Feld Beschreibung Endbetr�ge Wert �ndern proportionanl zum Wert $pdf->MultiCell(40
		$pdf->SetX(36);
		$temp = substr($order->totals[$i]['text'], 0, 3);
		if ($temp == '<b>') {
			$pdf->SetFont('Arial', 'B', 7);
			$temp2 = substr($order->totals[$i]['text'], 3);
			$order->totals[$i]['text'] = substr($temp2, 0, strlen($temp2) - 4);
		}
		$temp = substr($order->totals[$i]['title'], 0, 3);
		if ($temp == '<b>') {
			$pdf->SetFont('Arial', 'B', 7);
			$temp2 = substr($order->totals[$i]['title'], 3);
			$order->totals[$i]['title'] = substr($temp2, 0, strlen($temp2) - 5) . ':';
		}
		/* Small Workarround to remove '<' at 'Rechnungsbetrag<:' */
		if (strstr($order->totals[$i]['title'], '<'))
			$order->totals[$i]['title'] = str_replace ('<', '', $order->totals[$i]['title']);
		if (strstr($order->totals[$i]['text'], '<'))
			$order->totals[$i]['text'] = str_replace ('<', '', $order->totals[$i]['text']);
		/* end Workarround */
				
				
		// Breite Feld Beschreibung Endbetr�ge Wert �ndern proportionanl zum Wert $pdf->MultiCell(40
		$pdf->MultiCell(150, 4, $order->totals[$i]['title'], 0, 'R');
		$pdf->SetY(($Y_Table_Position + 2));
		$pdf->SetX(178);
		$pdf->MultiCell(25, 4, $order->totals[$i]['text'], 0, 'R');
		$Y_Table_Position += 4;
	}
	$Y_Table_Position += 4;
// Strich unter den Rechnungsbetr�gen
	//	$pdf->SetFont('Arial', 'B', 8);
	//	$pdf->SetTextColor(0);
	//	$pdf->SetY($Y_Table_Position);
	//	$pdf->SetX(20);
	//	$pdf->MultiCell(0, 0, "", 1, 'L', 1);
	//	$Y_Table_Position += 4;
	//	$x = 0;
	//	$y = $pdf->GetY();
	//	$pdf->SetLineWidth(0.25);
// $pdf->Line($x,$y,$this->w-$this->rMargin,$y);

	if ($Y_Table_Position > 240) {
		$pdf->AddPage();
		$Y_Table_Position = 70;
	}

// Bill-Amount-Tax-Detail-Box - 'Rechnungsbetrag setzt sich zusammen aus'-Box
// fuer 7% bzw 19% Mwst/USt Betraege
	
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$pdf->SetX(0);
	$pdf->SetY($Y_Table_Position - 20);
	$pdf->Cell(15);
	//$text2['edit_invoice_text'] = str_replace('<br>', '', str_replace('lalala.', '', $text2['edit_invoice_text']));
	$text = "Der Rechnugsbetrag setzt sich wie folgt zusammen:\n";
	foreach($invoice2->getTaxSums() as $rate => $value){
		//$n		= (float) $b / (((float)$rate /100) + 1);
		$n		= $value;
		$brutto = $value * (1+($rate/100));
		$tax	= $n * ($rate/100);
		$tax	= number_format(round($tax,2),2,',','.');
		$n		= number_format(round($n,2),2,',','.');
		$rate	= number_format(round($rate, 2),2,',','.');
		$brutto	= number_format(round($brutto, 2),2,',','.');
		$text .= "$rate % USt. aus                                $brutto EUR   $tax EUR  Netto = $n EUR \n";
	}
	if ($versandkosten){//todo
		$rate	= $invoice2->getTaxRateForShipping();
		$shipping = $invoice2->getShipping();
		$brutto = $shipping['brutto'];
		$netto	= $shipping['netto'];
		$tax	= $shipping['tax'];
		$rate	= number_format(round($rate, 2),2,',','.');
		$brutto	= number_format(round($brutto, 2),2,',','.');
		$netto	= number_format(round($netto,2),2,',','.');
		$tax	= number_format(round($tax,2),2,',','.');
		$text .= "$rate % USt. aus Nebenleistungen     $brutto EUR   $tax EUR   Netto = $netto EUR \n";
	}
	$pdf->MultiCell(180, 4, $text, 0, 'L');
	
// Rechnungstext 2 
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0);
	$pdf->SetX(0);
	$pdf->SetY($Y_Table_Position);
	$pdf->Cell(15);
	$text2['edit_invoice_text'] = str_replace('<br>', '', str_replace('Danke f�r Ihren Einkauf bei Kaffeesolo.', '', $text2['edit_invoice_text']));
	$pdf->MultiCell(180, 4, 'Danke f�r Ihren Einkauf bei Kaffeesolo.' . "\n" . $text2['edit_invoice_text'], 0, 'L');
	// hiernach kommt der Footer
}
$PdfName = "KAFFEESOLO_RN_";
$PdfType = "I";
if (isset($_GET["PdfName"]) && !empty($_GET["PdfName"])) {
	$PdfName = $_GET["PdfName"];
}
if (isset($_GET["PdfType"]) && !empty($_GET["PdfType"])) {
	$PdfType = $_GET["PdfType"];
}

//var_dump($order);
$pdf->Output($PdfName . $_GET["oID"], $PdfType);
?>


/* SAMPLE FOR $order */
/*
object(order)#4 (6) {
  ["info"]=>
  array(11) {
    ["currency"]=>
    string(3) "EUR"
    ["currency_value"]=>
    string(8) "1.000000"
    ["payment_method"]=>
    string(33) "<b>Bankeinzug per Lastschrift</b>"
    ["cc_type"]=>
    string(0) ""
    ["cc_owner"]=>
    string(0) ""
    ["cc_number"]=>
    string(0) ""
    ["cc_expires"]=>
    string(0) ""
    ["shipping_tax"]=>
    string(6) "0.0000"
    ["date_purchased"]=>
    string(19) "2012-06-24 20:55:35"
    ["orders_status"]=>
    string(1) "2"
    ["last_modified"]=>
    string(19) "2012-06-25 13:13:36"
  }
  ["totals"]=>
  array(5) {
    [0]=>
    array(6) {
      ["title"]=>
      string(15) "Zwischensumme: "
      ["text"]=>
      string(10) "126,99 EUR"
      ["class"]=>
      string(11) "ot_subtotal"
      ["value"]=>
      string(8) "126.9898"
      ["sort_order"]=>
      string(15) "Zwischensumme: "
      ["orders_total_id"]=>
      string(6) "213922"
    }
    [1]=>
    array(6) {
      ["title"]=>
      string(15) "Versandkosten: "
      ["text"]=>
      string(8) "3,90 EUR"
      ["class"]=>
      string(11) "ot_shipping"
      ["value"]=>
      string(6) "3.8999"
      ["sort_order"]=>
      string(15) "Versandkosten: "
      ["orders_total_id"]=>
      string(6) "213923"
    }
    [2]=>
    array(6) {
      ["title"]=>
      string(17) "Rechnungsbetrag<:"
      ["text"]=>
      string(10) "130,89 EUR"
      ["class"]=>
      string(8) "ot_total"
      ["value"]=>
      string(8) "130.8897"
      ["sort_order"]=>
      string(24) "<b>Rechnungsbetrag</b>: "
      ["orders_total_id"]=>
      string(6) "213924"
    }
    [3]=>
    array(6) {
      ["title"]=>
      string(23) "(enthaltene MwSt. 19%: "
      ["text"]=>
      string(9) "5,81 EUR)"
      ["class"]=>
      string(6) "ot_tax"
      ["value"]=>
      string(6) "5.8101"
      ["sort_order"]=>
      string(23) "(enthaltene MwSt. 19%: "
      ["orders_total_id"]=>
      string(6) "213925"
    }
    [4]=>
    array(6) {
      ["title"]=>
      string(22) "(enthaltene MwSt. 7%: "
      ["text"]=>
      string(9) "6,18 EUR)"
      ["class"]=>
      string(6) "ot_tax"
      ["value"]=>
      string(6) "6.1822"
      ["sort_order"]=>
      string(22) "(enthaltene MwSt. 7%: "
      ["orders_total_id"]=>
      string(6) "213926"
    }
  }
  ["products"]=>
  array(3) {
    [0]=>
    array(9) {
      ["qty"]=>
      string(1) "1"
      ["name"]=>
      string(42) "ECM Wasserfilter f. Ansaugschlauch 2er-Set"
      ["model"]=>
      string(4) "4054"
      ["tax"]=>
      string(7) "19.0000"
      ["tax_description"]=>
      string(20) "enthaltene MwSt. 19%"
      ["price"]=>
      string(7) "13.4370"
      ["final_price"]=>
      string(7) "13.4370"
      ["weight"]=>
      string(7) "0.33000"
      ["orders_products_id"]=>
      string(6) "109000"
    }
    [1]=>
    array(9) {
      ["qty"]=>
      string(1) "6"
      ["name"]=>
      string(44) "M�der Swiss Cafe Espresso Italy 1000g Bohnen"
      ["model"]=>
      string(4) "0142"
      ["tax"]=>
      string(6) "7.0000"
      ["tax_description"]=>
      string(19) "enthaltene MwSt. 7%"
      ["price"]=>
      string(7) "14.7196"
      ["final_price"]=>
      string(7) "14.7196"
      ["weight"]=>
      string(7) "0.99999"
      ["orders_products_id"]=>
      string(6) "109001"
    }
    [2]=>
    array(9) {
      ["qty"]=>
      string(1) "1"
      ["name"]=>
      string(44) "ECM Entkalker f�r Espressomaschinen 10 x 30g"
      ["model"]=>
      string(4) "4055"
      ["tax"]=>
      string(7) "19.0000"
      ["tax_description"]=>
      string(20) "enthaltene MwSt. 19%"
      ["price"]=>
      string(7) "13.8655"
      ["final_price"]=>
      string(7) "13.8655"
      ["weight"]=>
      string(7) "0.30000"
      ["orders_products_id"]=>
      string(6) "109002"
    }
  }
  ["customer"]=>
  array(12) {
    ["name"]=>
    string(16) "christoph momber"
    ["company"]=>
    string(11) "velo momber"
    ["street_address"]=>
    string(17) "landwehrstra�e 13"
    ["suburb"]=>
    string(0) ""
    ["city"]=>
    string(8) "w�rzburg"
    ["postcode"]=>
    string(5) "97070"
    ["state"]=>
    string(0) ""
    ["country"]=>
    string(11) "Deutschland"
    ["format_id"]=>
    string(1) "5"
    ["telephone"]=>
    string(12) "0177-3336947"
    ["customers_id"]=>
    string(5) "16259"
    ["email_address"]=>
    string(19) "info@velo-momber.de"
  }
  ["delivery"]=>
  array(9) {
    ["name"]=>
    string(16) "christoph momber"
    ["company"]=>
    string(11) "velo momber"
    ["street_address"]=>
    string(17) "landwehrstra�e 13"
    ["suburb"]=>
    string(0) ""
    ["city"]=>
    string(8) "w�rzburg"
    ["postcode"]=>
    string(5) "97070"
    ["state"]=>
    string(0) ""
    ["country"]=>
    string(11) "Deutschland"
    ["format_id"]=>
    string(1) "5"
  }
  ["billing"]=>
  array(9) {
    ["name"]=>
    string(16) "christoph momber"
    ["company"]=>
    string(11) "velo momber"
    ["street_address"]=>
    string(17) "landwehrstra�e 13"
    ["suburb"]=>
    string(0) ""
    ["city"]=>
    string(8) "w�rzburg"
    ["postcode"]=>
    string(5) "97070"
    ["state"]=>
    string(0) ""
    ["country"]=>
    string(11) "Deutschland"
    ["format_id"]=>
    string(1) "5"
  }
}


/* SAMPLE FOR $order */
/*
object(order)#3 (6) {
  ["info"]=>
  array(11) {
    ["currency"]=>
    string(3) "EUR"
    ["currency_value"]=>
    string(8) "1.000000"
    ["payment_method"]=>
    string(33) "<b>Bankeinzug per Lastschrift</b>"
    ["cc_type"]=>
    string(0) ""
    ["cc_owner"]=>
    string(0) ""
    ["cc_number"]=>
    string(0) ""
    ["cc_expires"]=>
    string(0) ""
    ["shipping_tax"]=>
    string(6) "0.0000"
    ["date_purchased"]=>
    string(19) "2012-06-24 20:55:35"
    ["orders_status"]=>
    string(1) "2"
    ["last_modified"]=>
    string(19) "2012-06-25 13:13:36"
  }
  ["totals"]=>
  array(5) {
    [0]=>
    array(6) {
      ["title"]=>
      string(15) "Zwischensumme: "
      ["text"]=>
      string(10) "126,99 EUR"
      ["class"]=>
      string(11) "ot_subtotal"
      ["value"]=>
      string(8) "126.9898"
      ["sort_order"]=>
      string(15) "Zwischensumme: "
      ["orders_total_id"]=>
      string(6) "213922"
    }
    [1]=>
    array(6) {
      ["title"]=>
      string(15) "Versandkosten: "
      ["text"]=>
      string(8) "3,90 EUR"
      ["class"]=>
      string(11) "ot_shipping"
      ["value"]=>
      string(6) "3.8999"
      ["sort_order"]=>
      string(15) "Versandkosten: "
      ["orders_total_id"]=>
      string(6) "213923"
    }
    [2]=>
    array(6) {
      ["title"]=>
      string(17) "Rechnungsbetrag<:"
      ["text"]=>
      string(10) "130,89 EUR"
      ["class"]=>
      string(8) "ot_total"
      ["value"]=>
      string(8) "130.8897"
      ["sort_order"]=>
      string(24) "<b>Rechnungsbetrag</b>: "
      ["orders_total_id"]=>
      string(6) "213924"
    }
    [3]=>
    array(6) {
      ["title"]=>
      string(23) "(enthaltene MwSt. 19%: "
      ["text"]=>
      string(9) "5,81 EUR)"
      ["class"]=>
      string(6) "ot_tax"
      ["value"]=>
      string(6) "5.8101"
      ["sort_order"]=>
      string(23) "(enthaltene MwSt. 19%: "
      ["orders_total_id"]=>
      string(6) "213925"
    }
    [4]=>
    array(6) {
      ["title"]=>
      string(22) "(enthaltene MwSt. 7%: "
      ["text"]=>
      string(9) "6,18 EUR)"
      ["class"]=>
      string(6) "ot_tax"
      ["value"]=>
      string(6) "6.1822"
      ["sort_order"]=>
      string(22) "(enthaltene MwSt. 7%: "
      ["orders_total_id"]=>
      string(6) "213926"
    }
  }
  ["products"]=>
  array(3) {
    [0]=>
    array(9) {
      ["qty"]=>
      string(1) "1"
      ["name"]=>
      string(42) "ECM Wasserfilter f. Ansaugschlauch 2er-Set"
      ["model"]=>
      string(4) "4054"
      ["tax"]=>
      string(7) "19.0000"
      ["tax_description"]=>
      string(20) "enthaltene MwSt. 19%"
      ["price"]=>
      string(7) "13.4370"
      ["final_price"]=>
      string(7) "13.4370"
      ["weight"]=>
      string(7) "0.33000"
      ["orders_products_id"]=>
      string(6) "109000"
    }
    [1]=>
    array(9) {
      ["qty"]=>
      string(1) "6"
      ["name"]=>
      string(44) "M�der Swiss Cafe Espresso Italy 1000g Bohnen"
      ["model"]=>
      string(4) "0142"
      ["tax"]=>
      string(6) "7.0000"
      ["tax_description"]=>
      string(19) "enthaltene MwSt. 7%"
      ["price"]=>
      string(7) "14.7196"
      ["final_price"]=>
      string(7) "14.7196"
      ["weight"]=>
      string(7) "0.99999"
      ["orders_products_id"]=>
      string(6) "109001"
    }
    [2]=>
    array(9) {
      ["qty"]=>
      string(1) "1"
      ["name"]=>
      string(44) "ECM Entkalker f�r Espressomaschinen 10 x 30g"
      ["model"]=>
      string(4) "4055"
      ["tax"]=>
      string(7) "19.0000"
      ["tax_description"]=>
      string(20) "enthaltene MwSt. 19%"
      ["price"]=>
      string(7) "13.8655"
      ["final_price"]=>
      string(7) "13.8655"
      ["weight"]=>
      string(7) "0.30000"
      ["orders_products_id"]=>
      string(6) "109002"
    }
  }
  ["customer"]=>
  array(12) {
    ["name"]=>
    string(16) "christoph momber"
    ["company"]=>
    string(11) "velo momber"
    ["street_address"]=>
    string(17) "landwehrstra�e 13"
    ["suburb"]=>
    string(0) ""
    ["city"]=>
    string(8) "w�rzburg"
    ["postcode"]=>
    string(5) "97070"
    ["state"]=>
    string(0) ""
    ["country"]=>
    string(11) "Deutschland"
    ["format_id"]=>
    string(1) "5"
    ["telephone"]=>
    string(12) "0177-3336947"
    ["customers_id"]=>
    string(5) "16259"
    ["email_address"]=>
    string(19) "info@velo-momber.de"
  }
  ["delivery"]=>
  array(9) {
    ["name"]=>
    string(16) "christoph momber"
    ["company"]=>
    string(11) "velo momber"
    ["street_address"]=>
    string(17) "landwehrstra�e 13"
    ["suburb"]=>
    string(0) ""
    ["city"]=>
    string(8) "w�rzburg"
    ["postcode"]=>
    string(5) "97070"
    ["state"]=>
    string(0) ""
    ["country"]=>
    string(11) "Deutschland"
    ["format_id"]=>
    string(1) "5"
  }
  ["billing"]=>
  array(9) {
    ["name"]=>
    string(16) "christoph momber"
    ["company"]=>
    string(11) "velo momber"
    ["street_address"]=>
    string(17) "landwehrstra�e 13"
    ["suburb"]=>
    string(0) ""
    ["city"]=>
    string(8) "w�rzburg"
    ["postcode"]=>
    string(5) "97070"
    ["state"]=>
    string(0) ""
    ["country"]=>
    string(11) "Deutschland"
    ["format_id"]=>
    string(1) "5"
  }
}

*/
*/