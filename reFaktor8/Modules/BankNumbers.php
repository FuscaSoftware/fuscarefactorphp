<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BankNumbers
 *
 * @author Fusca Software (fusca.de/) : Sebastian Braun
 */
class BankNumbers {
	public $kontonummer;
	public $blz;
	public $kontrollziffer;
	public $countryCode;
	public $iban;
	public $pruefcode;
	public $a_letters;
	
	public function __construct($defaultCountryCode = 'DE') {
		$letters = strtoupper("abcdefghijklmnopqrstuvwxyz");
		$this->a_letters = $this->getArrayForString($letters);
		$this->countryCode = $defaultCountryCode;
		
	}
	
	public function setOld($blz, $kontonummer) {
		$this->blz = $blz;
		$this->kontonummer = $kontonummer;
		return $this->checkOld();
	}
	public function checkOld(){
		return true;
	}
	
	/**
	 * BBAN is the IBAN without CountryCode and Checkchars
	 * @return string
	 */
	public function generateBBAN(){
		return $this->blz . $this->getKontonummer();
	}
	
	/**
	 * Fillup Kontonummer with zeros to get a String with length given in chars
	 * @param int $chars
	 * @return string|boolean(false)
	 */
	public function getKontonummer($chars = 10){
		$n = $this->kontonummer;
		$len = strlen($n);
		$missed = $chars - $len;
		if (!isset($missed) || !is_numeric($missed) || $missed < 0)
			return false;
		for ($index = 0; $index < $missed; $index++) {
			$n = "0".$n;
		}
		return $n;
	}
	
	public function getNumericCountryCode($countryCode){
		$letters = $this->getArrayForString($countryCode);
		foreach ($letters as $char) {
			$code[] = $this->getNumericByLetter($char);
		}
		return implode('', $code);
}
	/**
	 * 
	 * @param string $char
	 * @return int the key of the array;
	 */
	public function getNumericByLetter($char){
		$key = array_search($char, $this->a_letters);
		if (isset($key) && is_numeric($key) && !($key === false))
			return $key + 1 + 9;
		else
			return false;
	}
	
	public function getArrayForString($string){
		//$array = (array) $string;// Versuch1 = wirkungslos
		$array = preg_split('//', $string, -1, PREG_SPLIT_NO_EMPTY);//Versuch2
		return $array;
	}

	public function getPruefcode($countryCode, $bban){
		$pruefsumme = $bban.$this->getNumericCountryCode($countryCode)."00";
		/* Standard Rechenbereich von PHP reicht nicht für 24 Ziffern aus */
		//$check1		= (int) $pruefsumme % 97;
		if (!function_exists("bcmod")){
			echo "bcmod() ist nicht verfügbar!<br>\n";
			echo __DIR__."/".__FILE__.":".__LINE__."<br>\n";
			throw new Exception;
		}
		/* Als Alternative könnte man über eine SQL-Query rechnen. */
		/* $q = "SELECT ($pruefsumme % 97) AS m"; */
		$check1		= bcmod($pruefsumme, "97");
		$check2		= 98 - (int) $check1;
		if (is_numeric($check2) && $check2 < 10)
			return "0".(string) $check2;
		else if (is_numeric($check2) && $check2 >= 10 )
			return (string) $check2;
		else
			return false;
	}
	
	/**
	 * requires previous setOld() call !
	 * @return string in format Länderkennung +Prüfziffer + BBAN = IBAN
	 */
	public function generateIBAN(){
		if (empty($this->blz) || empty($this->kontonummer))
			return false;
		$bban = $this->generateBBAN();
		$this->iban = $this->countryCode.$this->getPruefcode($this->countryCode, $bban).$bban;
		return $this->iban;
	}
}

//$banknumbers = new BankNumbers();


/* Example from http://www.iban.de/iban-pruefsumme.html */
/*
$banknumbers->setOld("70090100", "1234567890");
echo $banknumbers->generateIBAN(); 
 //returns DE08700901001234567890
 * 
 */
