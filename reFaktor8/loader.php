<?php
/* load and initialize the framework */
require_once __DIR__.'/Core/index.php';
require_once __DIR__.'/App/Config.php';
require_once __DIR__.'/App/AppModel.php';
require_once __DIR__.'/App/AppController.php';
require_once __DIR__.'/App/AppView.php';
require_once __DIR__.'/App/Customer.php';

/* load Modules and Extensions for current Project */
//require_once __DIR__.'/App/Translation.php';