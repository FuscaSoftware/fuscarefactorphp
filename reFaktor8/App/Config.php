<?php
namespace reFaktor8;

class Config extends \reFaktor8\CoreConfig {

	private function databases() {
		$databases[] = (object) array(
					"host" => "localhost",
					"user" => "user",
					"password" => "password",
					"databasename" => "dbname",
					"type" => "mysqli",
		);
		return $databases;
	}

}
if (isset($reFaktor8))
	$reFaktor8->config = new \reFaktor8\Config();

?>