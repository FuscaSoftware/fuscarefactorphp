<?php
/**
 * Description of AppView
 *
 * @author Sebastian Braun
 */
class AppView extends \reFaktor8\View {
	public function __construct($objectName = null) {
		parent::__construct($objectName);
	}
	public function foreignScripts(){
		ob_start();
		?>
			 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script src="https://code.jquery.com/jquery.min.js"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
			<script type="text/javascript" language="javascript" src="includes/javascript/edit_orders.functions.js"></script>
		<?php
		return ob_get_clean();
	}
}
