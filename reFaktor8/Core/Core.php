<?php
namespace reFaktor8;
/**
 * 
 * 
 */
class reFaktor8 extends Adam {

	/** @var Config		config */
	public $config;
	public $model;
	public $view;
	public $controller;
	/**
	 * @var DBConnection	dbConnection 
	 * @var Input			input
	 */
	public $dbConnection;
	protected $dbParameters;
	public $input;
	public $errors;
	public $file;

	public function __construct($objectName = null) {
		parent::__construct($objectName);
		if (class_exists('Config', TRUE) || class_exists('\reFaktor8\Config', true))
			$this->config = new \reFaktor8\Config();
		/*
		else
			$this->config = new \reFaktor8\CoreConfig();
		 */
		$this->model = new \reFaktor8\Model();
		$this->view = new \reFaktor8\View();
		$this->controller = new \reFaktor8\Controller();
		$this->file = new \reFaktor8\File();
		$this->initializeInput();
	}
	
	public function __get($name) {
		parent::__get($name);
		$this->error(__CLASS__.' '.$name." konnte nicht gefunden werden.");
		$this->error(debug_backtrace());
	}
	
	public function dbConnection(){
		if ($this->dbConnection)
			return $this->dbConnection;
		else 
			return $this->initializeDBConnection ();
	}

	public function initializeDBConnection($parameters = null){
		if (!empty($parameters))
			$this->dbParameters = $parameters;
		elseif (!empty($this->dbParameters))
			$parameters = $this->dbParameters;
		elseif (empty($parameters) && !empty($this->config)){
				$parameters = $this->config->dbParams(); 
		}
		if ( !is_object($this->dbConnection) )
			$this->dbConnection = new \reFaktor8\DBConnection("DBConnection", $parameters);
		$this->model->dbConnection =& $this->dbConnection;
		return $this->dbConnection;
	}
	
	public function initializeInput(){
		if (!isset($this->input) && !is_object($this->input) )
			$this->input = new \reFaktor8\Input();
		return $this->input;
	}
	
	public function loadModel($model){
		if (!isset($this->model->$model)) {
			$class = ucfirst($model);
			if (class_exists($class)){
				if (!@$this->model->$model = new $class($this->model->dbConnection)){
				}
			}
			elseif (class_exists("\\reFaktor8\\".$class )){
				$class = "\\reFaktor8\\".$class;
				$this->model->$model = new $class($this->model->dbConnection);
			}
			else {
				$this->model->$model = new \reFaktor8\Model($this->model->dbConnection);
				$this->model->$model->table = strtolower($model);
			}
		}
		elseif (!is_object($this->model->$model)){
			$this->error("Fehler mit dem Model: $model!", __CLASS__.__METHOD__);
		}
		return $this->model->$model;
	}
	
	/**
	 * 
	 * @return String in lowercase
	 */
	public function getServerName(){
		return strtolower($_SERVER['SERVER_NAME']);
	}

	/**
	 * 
	 * @return String $_SERVER['REQUEST_URI'}
	 */
	public function getRequestUri(){
		return $_SERVER['REQUEST_URI'];
	}
	
	/**
	 * 
	 * @return string
	 */
	public function endUp(){
		if (is_array($this->errors)){
			$box = "\n\n<div class='log'>\n";
			foreach ($this->errors as $e)
				$box .= "<div><pre>\n".($e)."\n</pre></div>\n";
			$box .= "\n</div>\n";
		}
		return $box;
	}
}

/**
 * Adam is the first class of all other classes.
 * 
 */
class Adam {
	public function __construct($objectName = null) {
		if (!empty($objectName))
			$this->__name = $objectName;
	}
	protected $errors;

	public function __destruct() {
		if (!empty($this->errors))
			$this->error($this->errors);
	}
	
	/**
	 *
	 * @var String __name
	 */
	protected $__name;
	public function __name(){
		if (isset($this->__name) && !empty($this->name))
			return $this->__name;
		else
			return false;
	}
	public function __get($name) {
		$this->error(debug_backtrace());
		if (isset($this->$name))
			if (is_object($this->$name))
				return $this->$name;
			else
				return ;
	}

	public function error($message, $location = null){
		global $reFaktor8;
		if (!is_string($message)){
			ob_start();
			$var = func_get_args();
			call_user_func_array('var_dump', $var);
			$message = htmlentities(ob_get_clean());
		}
		if ($reFaktor8 && ( $reFaktor8 != $this) )
			$reFaktor8->error($message, $location);
	
		if ($location)
			$message = $location." :: \n".$message;
		$this->errors[] = $message;
//		echo $message;
	}
}

class Model extends Adam {
	/** @var DBConnection DBConnection
	 *  @var String			table
	 */
	public	$dbConnection;
	protected $table = false;
	protected $lastSQL;
	
	
	/**
	 * 
	 * @param DBConnection $dbConnection
	 */
	function __construct(&$dbConnection = null, $name = null) {
		global $reFaktor8;
		if ($dbConnection == null){
			if ($reFaktor8){
				$this->dbConnection =& $reFaktor8->dbConnection();
			}
		}
		else 
			$this->dbConnection =& $dbConnection;
		$class = get_class($this);
		if ($class != 'reFaktor8\\Model' && empty($this->table))
			$this->table = strtolower($class);
		parent::__construct($name);
	}
	
	/**
	 * @return array associative Array
	 */
	public function getAssoc($sqlOrResult){
		$result = $this->dbConnection->getAssoc($sqlOrResult);
		return $result;
	}
	
	/**
	 * 
	 * @param type $sqlOrResult
	 * @return array of objects Objekt
	 */
	public function getObjects($sqlOrResult){
		if (empty($sqlOrResult)){
			echo __Funktion__."Parameter sind leer!\n";
			$this->error(debug_backtrace());
		}
		$objects = $this->dbConnection->getObjects($sqlOrResult);
		if (!is_array($objects))
			$this->error(array("obj" => $objects,"sqlOrResult" => $sqlOrResult, /*"this->dbConnection" => $this->dbConnection*/), "Model->getObject()");
		return $objects;
	}
	
	public function getO($sqlOrResult){
		$objects = $this->getObjects($sqlOrResult);
		if (isset($objects) && is_array($objects) && isset($objects[0])){
			$obj1 = $objects[0];
			if ($obj1)
				return $obj1;	
		}
	}
	
	public function find($fields = "*", $where = "1", $limit = "1000"){
		$res = $this->select($fields, $where, $limit);
		if ($res)
			return $this->getObject($res);
	}
	
	/**
	 * 
	 * @param type $fields
	 * @param type $where
	 * @param type $limit
	 * @return array of objects
	 */
	public function findAll($fields = "*", $where = "1", $limit = "1000"){
		$array = array();
		$result = $this->select($fields, $where, $limit);
		if ($result)
			$array = $this->getObjects ($result);
		return $array;
	}
	/**
	 * Returns the last sql-Query-String
	 * @return String sql
	 */
	public function lastSQL(){
		return $this->lastSQL;
	}


	/**
	 * 
	 * @param type $fields
	 * @param type $where
	 * @return resource resource
	 */
	public function select($fields = "*", $where = "1", $limit = "1000"){
		if (!$this->table)
			return FALSE;
		$q = "SELECT $fields FROM $this->table WHERE $where LIMIT $limit";
		$this->lastSQL = $q;
		$r = $this->query($q);
		if (empty($r) or 1)
			$this->error($r, "Model->select(), $q");
		return $r;
	}
	public function select2($params){
		return $this->select($params['fields'], $params['where'], $params['limit']);
	}
	
	public function delete($id){
		if (!$this->table)
			return false;
		$sql = "delete from $this->table where id='" . $id . "'";
		return $this->query($sql);
	}
	
	/**
	 * 
	 * @param String $sql
	 * @return resource resource
	 */	
	public function query($sql){
//		echo var_dump(debug_backtrace());
//		echo $sql;
		$this->lastSQL = $sql;
		$r = $this->dbConnection->query($sql);
		$this->error($r, "$this->__name() Model->query() - SQL: $sql");
        return $r;
	}
	
		/**
	 * 
	 * @param array|object $array
	 * @return string in fortmat of json
	 */
	public function array2json($array){
		$elements = array();
		foreach ($array as $key => $value){
			if (is_array($value) || is_object($value))
				$value = $this->array2json ($value);
			else
				$value = "\"$value\"";
			$elements[] = " \"$key\" : $value ";
		}
		$content = implode (",", $elements);
		$json = "{".$content."}";
		return $json;
	}
	
	/**
	 * Alias for array2json
	 * @param type $object
	 */
	public function object2json($object){
		return $this->array2json($object);
	}
	
	/**
	 * Finds Numbers in Text e.g.: "Es gibt 2 % Rabatt." returns 2
	 * @param type $str
	 * @return type
	 */
	public function getNumerics ($str) {
        //preg_match_all('/\d+/', $str, $matches);
        //preg_match_all('!\d+!', $str, $matches);
        //return implode("", $matches);
		$result = array();
		// Check each character.
		$len = strlen($str);
		for($i = 0; $i < $len; $i++) {
			if(is_numeric($str[$i])) {
				$result[] = $str[$i];
			}
		}
		return implode("", $result);		
    }
}

class View extends Adam {
	public $path;

	function __construct($objectName = null) {
		parent::__construct($objectName);
		$this->path = __Dir__.'/template/elements/';
	}
	public function element($name){
		$filename = $this->path.$name;
		if (file_exists($filename.'.php'))
			$filename = $filename.'.php';
		elseif (file_exists($filename.'.html'))
			$filename = $filename.'.html';
		include $filename;
		
	}
}

class Controller extends Adam {

	function __construct($objectName = null) {
		parent::__construct($objectName);
	}

}

class DBConnection extends Adam {

	public $connections;
	public $defaultConnection;
	public $databases;
	protected $type;

	public function __construct($objectName = null, $parameters = null, $type = 'mysqli') {
		parent::__construct($objectName);
		if (!empty($parameters))
			$this->dbConnect ($parameters);
	}
	
	/**
	 * 
	 * @param object $parameters-object or instance of Config
	 * @return connection
	 */
	public function dbConnect($parameters){
		if (isset($parameters->type)){
			$this->type	= $parameters->type;
		}
		else
			$this->error ("Es wurde kein Datenbanktyp definiert!", __CLASS__."->".__METHOD__.":".__LINE__);
		
		switch ($this->type) {
			case 'none':
				break;
			case 'mysql':
				$connection = $this->connectMySQL($parameters);
				break;
			case 'mysqli':
			default:
				$connection = $this->connectMySQLi($parameters);
				break;
		};

		if (isset($connection) && $connection)
			$this->connections[] = $connection;
		if (empty($this->defaultConnection))
			$this->defaultConnection = $this->connections[count($this->connections)-1];
		
		if (isset($parameters->database))
			$this->selectDB ($parameters);
		return $connection;
	}
	
	public function selectDB($parameters, $connection = null){
		if (empty($connection))
			$connection = $this->defaultConnection;
		switch ($this->type){
			case 'mysql':
				$r = mysql_select_db($parameters->database, $connection) or die("selectDB() Fehler: " . mysql_error());
		}
		if ($r)
			$this->databases[] = $parameters->database;
	}


	public static function parameters($server, $username, $password, $database = null, $type = 'mysqli'){
		$p = new \stdClass();
		$p->server		= $server;
		$p->username	= $username;
		$p->password	= $password;
		$p->database	= $database;
		$p->type		= $type;
		return $p;
	}

	public function connectMySQL ($params){
		$connection = mysql_connect($params->server, $params->username, $params->password) or die("connectMySQL() Fehler: " . mysql_error());
		return $connection;
	}
	
	private function connectMySQLi($params) {
		
	}
	
	/**
	 * send the query to the database, nomaly it uses mysql_query($sql)
	 * @param String $sql
	 * @return resource
	 */
	public function query($sql){
		if (empty($sql))
			$this->error("Sie haben query() keinen Befehl mitgegeben!");
//		var_dump($this->type);
		switch ($this->type){
			case 'mysql':
				$r = mysql_query($sql) or die("query( $sql ) Fehler: ".mysql_errno());
				$this->error(mysql_affected_rows(), "affected_rows for $sql");
				break;
			default :
				$this->error($sql." failed", __CLASS__.__METHOD__);
				$this->error("Dieser Datenbank-Typ $this->type wird leider nicht unterstützt!", __CLASS__.__METHOD__);
		}
//		var_dump($r);
		if (@$r)
			return $r;
		else
			$this->error(__Class__." -> ".__METHOD__." ergab für die folgende Query leider keine Ergebnisse: $sql");
	}

	/**
	 * 
	 * @param type $sqlOrResult
	 * @return array AssocArray
	 * 
	 */
	public function getAssoc($sqlOrResult){
		if (empty($sqlOrResult))
			echo __Function__."Parameter sind leer!\n";
		$result = $this->sqlOrResult($sqlOrResult);

		switch ($this->type){
			case "mysql":
				$a = mysql_fetch_assoc($result);
		}
		return $a;
	}
	
	/**
	 * 
	 * @param type $sqlOrResult
	 * @return objects Array of Objects
	 */
	public function getObjects($sqlOrResult){
		if (empty($sqlOrResult)){
			echo __Function__."Parameter sind leer!\n";
			return false;
		}
		$result = $this->sqlOrResult($sqlOrResult);
		$objects = array();
		
		if (!is_string($result) && !empty($result)){
			switch ($this->type){
				case 'mysql':
					while ($singleObject = mysql_fetch_object($result)) {
						$objects[] = $singleObject;
					}
					break;
				default:
					$this->error("Dieser Datenbanktyp ($this->type) wird nicht unterstützt!", __CLASS__.__METHOD__);
					break;
			}
		}
		if (!is_array($objects))
			$this->error(array("obj" => $objects,"sqlOrResult" => $sqlOrResult, "result" => $result, /*"this->dbConnection" => $this->dbConnection*/), "DBConnection->getObject()");
		else 
			$this->error(array("obj" => $objects,"sqlOrResult" => $sqlOrResult, "result" => $result), "DBConnection->getObject() - Alles gut!");
		return $objects;
	}
	
	private function sqlOrResult($sqlOrResult){
		if (empty($sqlOrResult))
			echo __Function__."Parameter sind leer!\n";
		if (is_string($sqlOrResult) )
			$result = $this->query($sqlOrResult);
		elseif (strstr(gettype($sqlOrResult),"resource"))
			$result = $sqlOrResult;
		elseif ($sqlOrResult == null)
			$this->error("\n".__Class__." ".__Line__."Fehler: Das sollte nicht passieren!<br/>\n". /*var_export(debug_backtrace(), 1).*/"\n");
		if (empty($result) or 1)
			$this->error(array("sqlOrResult" => $sqlOrResult, "result" => $result, /*"this->dbConnection" => $this->dbConnection*/), "DBConnection->sqlOrResult()");
		return $result;
	}
	
}

abstract class CoreConfig extends Adam {

	public $databases;

	function __construct($objectName = null) {
		$this->databases = $this->databases();//warum funktioniert diese Zeile nicht?!
		parent::__construct($objectName = null);
	}

	/**
	 * this function have to be overwritten by Config (in App).
	 */
	protected function databases() {
		echo "Override!\n";
	}
	public function dbParams(){
		$db = $this->databases();
		if (is_array($db))
			return $db[0];
		else false;
	}
	
}

class Input	extends Adam {
	
	
	function __construct($objectName = null) {
		parent::__construct($objectName);
	}
	/**
	 * function from spritpsonsoren.com
	 */
	public function filterAll(){
		foreach ($_POST as $key => $value) {
			$_POST[$key] = $this->filter($value);
		}
		
		foreach ($_FILES as $key => $value) {
			$_FILES[$key]['name'] = $this->filter($_FILES[$key]['name']);
		}
	}
	/**
	 * @param type $data
	 * @return type
	 */
	public function filter($data) {
		$data = trim($data);
		if (get_magic_quotes_gpc())
			$data = stripslashes($data);
		$data = mysql_real_escape_string($data);
		return $data;
	}
	
	public function post($name){
		if (isset($_POST[$name]))
			return mysql_real_escape_string($_POST[$name]);
		else
			return false;
	}
	
	public function get($name){
		if (isset($_GET[$name]))
			return mysql_real_escape_string($_GET[$name]);
		else
			return false;
	}
	
	public function session($name){
		if (isset($_SESSION[$name]))
			return mysql_real_escape_string ($_SESSION[$name]);
		else
			return FALSE;
	}
}

class Dispatcher extends Adam {

	function __construct($objectName = null) {
		parent::__construct($objectName);
	}

}

class File extends Adam{
	public function __construct($objectName = null) {
		parent::__construct($objectName);
	}
	public $append = 0;
	public $success;
	public function write2file($filename, $text){
		error_reporting(1);
		if ($this->append)
			$flags = FILE_APPEND;
		$bytes = file_put_contents($filename, $text, $flags);
		if ($bytes > 0)
			$this->success = TRUE;
		else {
			$this->success = FALSE;
			$this->error(array("filename" => $filename, /*"text" => $text*/), __CLASS__.__METHOD__." failed");
		}
		return $this->success = true;
	}
	public function getErrors(){
		return implode("\n<br/>", $this->errors);
	}
}

?>